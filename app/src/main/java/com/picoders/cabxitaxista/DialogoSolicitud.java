package com.picoders.cabxitaxista;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by juancho on 19/11/16.
 */

public class DialogoSolicitud extends Activity implements Solicitud.OnEstadoChangedListener{

    ImageView imageusuario;
    TextView textousuario,textodireccion,botonaceptar,botoncancelar;
    Solicitud SOLICITUD;
    String idTaxi,idTaxista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialogo_solicitud);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        this.setFinishOnTouchOutside(false);

        imageusuario = (ImageView) findViewById(R.id.imageusuario);
        textousuario = (TextView) findViewById(R.id.textousuario);
        textodireccion = (TextView) findViewById(R.id.textodireccion);
        botonaceptar = (TextView) findViewById(R.id.botonaceptarsolicitud);
        botoncancelar = (TextView) findViewById(R.id.botoncancelarsolicitud);

        SOLICITUD = (Solicitud) getIntent().getParcelableExtra("solicitud");
        SOLICITUD.escucharCambiosDeEstado(this);

        if(SOLICITUD.TIPOSOLICITUD.equals("normal")){
            if(SOLICITUD.USUARIO!=null) {
                if(SOLICITUD.USUARIO.IMAGEN!=null)
                Picasso.with(this).load(SOLICITUD.USUARIO.IMAGEN).transform(new CircleTransform()).fit().into(imageusuario);
                else
                    Picasso.with(this).load(R.drawable.perfil).transform(new CircleTransform()).fit().into(imageusuario);
                textousuario.setText(SOLICITUD.USUARIO.NOMBRE + " " + SOLICITUD.USUARIO.APELLIDO);
            }
            }else{
            if(SOLICITUD.EMPRESA!=null)
            {
                if(SOLICITUD.EMPRESA.imagen.length()>0) {
                    Picasso.with(this).load(SOLICITUD.EMPRESA.imagen).transform(new CircleTransform()).fit().into(imageusuario);
                }else
                    Picasso.with(this).load(R.drawable.logo).transform(new CircleTransform()).fit().into(imageusuario);
                textousuario.setText(SOLICITUD.EMPRESA.nombre);
            }
        }
        idTaxi = SOLICITUD.IDTAXI;
        idTaxista = SOLICITUD.IDTAXISTA;
        textodireccion.setText(SOLICITUD.DIRECCION);

        botoncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SOLICITUD.IDTAXISTA!=null){
                    FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID).child("estado").setValue("cancelado");
                }
                finish();

            }
        });
        botonaceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatabaseReference solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID);
                Map<String,Object> taskMap = new HashMap<String,Object>();
                taskMap.put("idTaxi", SOLICITUD.TAXI.ID);
                taskMap.put("idTaxista",SOLICITUD.TAXISTA.ID);
                taskMap.put("millisAceptado",System.currentTimeMillis());
                taskMap.put("estado","aceptado");
                solicitudref.updateChildren(taskMap);

               // lanzarmapaCarrera();
                //TODO lanzar mapacarrera

            }
        });

        wakeUpScreen();
        releaseLock();
    }
    private void wakeUpScreen(){
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire();
    }

    private void releaseLock(){
        KeyguardManager keyguardManager = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
    }
    public void lanzarmapaCarrera(){
        Intent intentomapacarrera = new Intent(this,MapaCarrera.class);
       intentomapacarrera.putExtras(SOLICITUD.BUNDLE);
        intentomapacarrera.putExtra("nombreUsuario",SOLICITUD.USUARIO.NOMBRE+" "+SOLICITUD.USUARIO.APELLIDO);
        intentomapacarrera.putExtra("imagenUsuario",SOLICITUD.USUARIO.IMAGEN);
        startActivity(intentomapacarrera);
    }

    @Override
    public void estadoChanged(Solicitud solicitud) {
        if (!solicitud.ESTADO.equals("pendiente"))
            finish();
    }

    @Override
    public void onTaxistaFinishedLoading(Taxista taxista) {

    }



    @Override
    public void onUsuarioFinishedLoading(Usuario usuario, Solicitud solicitud) {
        Picasso.with(this).load(usuario.IMAGEN).transform(new CircleTransform()).fit().into(imageusuario);
        textousuario.setText(usuario.NOMBRE+" "+usuario.APELLIDO);

    }

    @Override
    public void onEmpresaFinishedLoading(Empresa empresa, Solicitud solicitud) {
        if(empresa.imagen.length()>0)
        Picasso.with(this).load(empresa.imagen).transform(new CircleTransform()).fit().into(imageusuario);
        textousuario.setText(empresa.nombre);
    }

    @Override
    public void onTaxiFinishedLoading(Taxi taxi) {

    }
}
