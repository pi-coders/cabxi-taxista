package com.picoders.cabxitaxista;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by juancho on 17/11/16.
 */

public class ListaTaxisHolder extends RecyclerView.ViewHolder {
    ImageView image;
    TextView texto;
    Context context;
    Taxi taxi;
    View item;
    public ListaTaxisHolder(View itemView) {
        super(itemView);
        this.image = (ImageView) itemView.findViewById(R.id.imagenarca);
        this.texto = (TextView) itemView.findViewById(R.id.textotaxi);
        this.item=itemView;

    }
    public void pintarLinea(Context context, final Taxi taxi){
        this.context=context;
        this.taxi=taxi;
        if(taxi.IMAGENMARCA!=null)
        Picasso.with(context).load(taxi.IMAGENMARCA.replace(" ","%20")).fit().into(image);
        texto.setText("Placa: "+taxi.PLACA+"  Modelo: "+taxi.MODELO);
        final AutorizedTaxiClickListener listener = (AutorizedTaxiClickListener)context;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAutorizedTaxiClicked(taxi);
            }
        });
    }
   public interface AutorizedTaxiClickListener{
        void onAutorizedTaxiClicked(Taxi taxi);
    }
}

