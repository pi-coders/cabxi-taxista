
package com.picoders.cabxitaxista;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by juancho on 4/11/16.
 */

public class Taxista implements Parcelable{
    String CIUDAD,CODIGO_EMPRESA,DOCUMENTO,ESTADO,IMAGEN,NOMBRE,TELEFONO,ID;
    MarkerOptions MARKER;
    Context context;
    double CURRENT_LATITUD,CURRENT_LONGITUD;
    LatLng LATLNG;
    onTaxistaListener listener;


    public Taxista(Context context,Bundle bundle){
        this.CIUDAD = bundle.getString("ciudad");
        this.CODIGO_EMPRESA=bundle.getString("codigoEmpresa");
        this.DOCUMENTO = bundle.getString("documento");
        this.ESTADO=bundle.getString("estado");
        if(ESTADO==null)
            this.ESTADO="libre";
        this.NOMBRE=bundle.getString("nombre");
        this.IMAGEN=bundle.getString("imagen");
        this.TELEFONO=bundle.getString("telefono");
        this.ID=bundle.getString("idTaxista");
        this.context=context;


    }

    protected Taxista(Parcel in) {
        CIUDAD = in.readString();
        CODIGO_EMPRESA = in.readString();
        DOCUMENTO = in.readString();
        ESTADO = in.readString();
        IMAGEN = in.readString();
        NOMBRE = in.readString();
        TELEFONO = in.readString();
        ID = in.readString();
        MARKER = in.readParcelable(MarkerOptions.class.getClassLoader());
        CURRENT_LATITUD = in.readDouble();
        CURRENT_LONGITUD = in.readDouble();
        LATLNG = in.readParcelable(LatLng.class.getClassLoader());

    }

    public static final Creator<Taxista> CREATOR = new Creator<Taxista>() {
        @Override
        public Taxista createFromParcel(Parcel in) {
            return new Taxista(in);
        }

        @Override
        public Taxista[] newArray(int size) {
            return new Taxista[size];
        }
    };

    public void setLatLng(double latitud, double longitud, Context ctx){
        this.CURRENT_LATITUD=latitud;
        this.CURRENT_LONGITUD=longitud;
        this.LATLNG = new LatLng(latitud,longitud);
        if(MARKER==null)
            createMarker(ctx);

    }
    public Bundle getBundle(){
        Bundle bundle = new Bundle();
        bundle.putString("ciudad",CIUDAD);
        bundle.putString("codigoEmpresa",CODIGO_EMPRESA);
        bundle.putString("documento",DOCUMENTO);
        bundle.putString("estado",ESTADO);
        bundle.putString("nombre",NOMBRE);
        bundle.putString("imagen",IMAGEN);
        bundle.putString("telefono",TELEFONO);
        bundle.putString("idTaxista",ID);
        return bundle;
    }
    private void createMarker(final Context ctx){
        MARKER = new MarkerOptions().position(new LatLng(CURRENT_LATITUD, CURRENT_LONGITUD));
        Picasso.with(context).load(R.drawable.taxi).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, 50, 50, false);
                MARKER.icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).anchor(0.5f,0.5f);
                listener = (onTaxistaListener)ctx;
                listener.onMarkerCreated();


            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }
    public void escucharCambiosEstado(final Context ctx){
        DatabaseReference taxistaref = FirebaseDatabase.getInstance().getReference().child("taxistas").child(ID);
        taxistaref.child("estado").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener = (onTaxistaListener)ctx;
                if(dataSnapshot.getValue()!=null) {
                    listener.onTaxistaEstadoChanged(dataSnapshot.getValue().toString());
                    ESTADO = dataSnapshot.getValue().toString();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
     public void cambiarEstado(String estado){

         DatabaseReference taxistaref = FirebaseDatabase.getInstance().getReference().child("taxistas").child(ID);
         if(ESTADO.equals(estado)){
             taxistaref.child("estado").setValue("tempporal");
         }
         taxistaref.child("estado").setValue(estado);
     }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CIUDAD);
        dest.writeString(CODIGO_EMPRESA);
        dest.writeString(DOCUMENTO);
        dest.writeString(ESTADO);
        dest.writeString(IMAGEN);
        dest.writeString(NOMBRE);
        dest.writeString(TELEFONO);
        dest.writeString(ID);
        dest.writeParcelable(MARKER, flags);
        dest.writeDouble(CURRENT_LATITUD);
        dest.writeDouble(CURRENT_LONGITUD);
        dest.writeParcelable(LATLNG, flags);

    }

    public interface onTaxistaListener{
        void onMarkerCreated();
        void onTaxistaEstadoChanged(String estado);
    }
}

