package com.picoders.cabxitaxista;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by juancho on 4/11/16.
 */

public class SolicitudesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Solicitud> lista;
    Context context;
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.solicitudes_row, parent, false);
        return new ColicitudesHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Solicitud solicitud = lista.get(position);
        ColicitudesHolder holder1 = (ColicitudesHolder)holder;
        holder1.pintarLinea(solicitud,context);

    }

    @Override
    public int getItemCount() {

        return lista.size();
    }
    public SolicitudesAdapter(Context context,List<Solicitud> lista){
        this.context=context;
        this.lista=lista;

    }
}
