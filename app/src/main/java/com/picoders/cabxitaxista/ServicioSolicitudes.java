package com.picoders.cabxitaxista;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 4/11/16.
 */

public class ServicioSolicitudes extends Service implements ChildEventListener, LocationListener, GoogleApiClient.ConnectionCallbacks, Solicitud.OnEstadoChangedListener {
    DatabaseReference solicitudref;
    Location currentlocation;
    private static int DISTANCIA_DESCUBIR = 1500;
    private static int TIMEOUT_REPORTE_SEGUNDOS = 5;
    private static int SEGUNDOS_DE_MUESTRA_NOTIFICACION = 120; //2 Minutos
    long ULTIMO_REPORTE_POSICION = 0;
    private static int IDENT_NOTIF = 1;
    Taxi TAXI;
    Taxista TAXISTA;
    String CIUDAD;
    LocationRequest locationRequest;
    GoogleApiClient googleApiClient;
    Notification notification;
    Narracion narracion;
    DatabaseReference taxiref;
    List<String> SOLICITUDES_PASADAS = new ArrayList<>();
    Solicitud CURRENT_SOLICITUD;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            DISTANCIA_DESCUBIR = intent.getIntExtra("distancia_descubrir", 1500);

            TAXI = intent.getExtras().getParcelable("taxi");
            TAXISTA = intent.getExtras().getParcelable("taxista");
            CIUDAD = TAXISTA.CIUDAD;
            solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes");
            // Query querysol = solicitudref.orderByChild("estado").equalTo("pendiente");
            taxiref = FirebaseDatabase.getInstance().getReference().child("taxis").child(TAXI.ID);
            taxiref.child("disponible").setValue(true);
            solicitudref.addChildEventListener(this);
            currentlocation = new Location("currentLocation");
            currentlocation.setLatitude(TAXI.LATITUD);
            currentlocation.setLongitude(TAXI.LONGITUD);
            narracion = new Narracion(this);



            Intent normalIntent = new Intent(this, MainMap.class);
            normalIntent.putExtra("taxista", TAXISTA);
            normalIntent.putExtras(TAXI.BUNDLE);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                    normalIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            notification = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_stat_name)  // the status icon
                    .setTicker(getString(R.string.recibiento_solicitudes))  // the status text
                    .setWhen(System.currentTimeMillis())  // the time stamp
                    .setContentTitle(getText(R.string.recibiento_solicitudes))
                    .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                    .build();

            startForeground(IDENT_NOTIF, notification);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Bundle bundle = new Bundle();
        for (DataSnapshot snap : dataSnapshot.getChildren())
            bundle.putString(snap.getKey(), snap.getValue().toString());
        bundle.putString("idSolicitud", dataSnapshot.getKey());


        if (!SOLICITUDES_PASADAS.contains(dataSnapshot.getKey())) {
            SOLICITUDES_PASADAS.add(dataSnapshot.getKey());
            bundle.putString("currentTaxista", TAXI.IDTAXISTA);
            bundle.putString("currentTaxi", TAXI.ID);

            String estado = bundle.getString("estado");

            if (estado != null) {

                try {
                    Location locsolicitud = new Location(bundle.getString("idSolicitud"));
                    locsolicitud.setLatitude(Double.parseDouble(bundle.getString("latitud")));
                    locsolicitud.setLongitude(Double.parseDouble(bundle.getString("longitud")));
                    float distance = currentlocation.distanceTo(locsolicitud);
                    long pedido = Long.parseLong(bundle.getString("millisPedido"));
                    long diference = System.currentTimeMillis() - (SEGUNDOS_DE_MUESTRA_NOTIFICACION * 1000);

                    if (bundle.getString("idTaxista") == null && distance <= DISTANCIA_DESCUBIR && estado.equals("pendiente") && pedido > diference) {
                        CURRENT_SOLICITUD = new Solicitud(ServicioSolicitudes.this,bundle);
                       // CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                        if(CURRENT_SOLICITUD.TIPOSOLICITUD.equals("normal")){
                            if(CURRENT_SOLICITUD.USUARIO==null)
                                CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                        }else{
                            if(CURRENT_SOLICITUD.EMPRESA==null)
                                CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                        }

                        //lanzarDialogo(CURRENT_SOLICITUD);

                    } else if (bundle.getString("idTaxista") != null)
                        if (bundle.getString("idTaxista").equals(TAXI.IDTAXISTA) && estado.equals("pendiente")) {
                            CURRENT_SOLICITUD = new Solicitud(ServicioSolicitudes.this,bundle);
                            if(CURRENT_SOLICITUD.TIPOSOLICITUD.equals("normal")){
                                if(CURRENT_SOLICITUD.USUARIO==null)
                                    CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                            }else{
                                if(CURRENT_SOLICITUD.EMPRESA==null)
                                    CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                            }

                            try {
                                Thread.sleep(750);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            // lanzarDialogo(CURRENT_SOLICITUD);
                        } else if (bundle.getString("idTaxista").equals(TAXI.IDTAXISTA) && !estado.contains("cancela") && !estado.equals("destino")) {
                            CURRENT_SOLICITUD = new Solicitud(ServicioSolicitudes.this,bundle);
                            if(CURRENT_SOLICITUD.TIPOSOLICITUD.equals("normal")){
                                if(CURRENT_SOLICITUD.USUARIO==null)
                                    CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                            }else{
                                if(CURRENT_SOLICITUD.EMPRESA==null)
                                    CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                            }
                            try {
                                Thread.sleep(750);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }else if(bundle.getString("idTaxista").equals(TAXI.IDTAXISTA) && estado.equals("destino")){
                            CURRENT_SOLICITUD = new Solicitud(ServicioSolicitudes.this,bundle);
                            if(CURRENT_SOLICITUD.TIPOSOLICITUD.equals("normal")){
                                if(CURRENT_SOLICITUD.USUARIO==null)
                                    CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                            }else{
                                if(CURRENT_SOLICITUD.EMPRESA==null)
                                    CURRENT_SOLICITUD.obtenerUsuario(ServicioSolicitudes.this);
                            }

                        }
                }catch (NumberFormatException e){

                }catch (NullPointerException e){

                }

                //se activa el callback de onempresaloaded y onusurarioloaded


            }



        }

    }

    private void sendBroadcast(Solicitud solicitud){
        Intent intentosol = new Intent("nuevasolicitud");
        intentosol.putExtra("solicitud", solicitud);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentosol);
    }

    private void sendTodasSolicitudesDeTaxista(Solicitud solicitud){
        Intent intentosol = new Intent("todassolicitudes");
        intentosol.putExtra("solicitud", solicitud);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentosol);
    }

    private void lanzarDialogo(final Solicitud solicitud) {
        Intent intentodialogo = new Intent(this, DialogoSolicitud.class);
        solicitud.TAXI = TAXI;
        solicitud.TAXISTA=TAXISTA;
      //  solicitud.IDTAXISTA=TAXISTA.ID;
        solicitud.IDTAXI=TAXI.ID;
        intentodialogo.putExtra("solicitud",solicitud);
        intentodialogo.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intentodialogo);
        narracion.decir(solicitud.DIRECCION);
        SOLICITUDES_PASADAS.add(solicitud.ID);


    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    public void onLocationChanged(Location location) {
        currentlocation = location;
/*        if(ULTIMO_REPORTE_POSICION < (System.currentTimeMillis()-(TIMEOUT_REPORTE_SEGUNDOS+1000))){
        //    TAXI.cambiarPosicion(location);
            ULTIMO_REPORTE_POSICION=System.currentTimeMillis();
        }*/
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        while(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000); // Update location every 5 second
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        narracion.apagar();
        startForeground(0,notification);
        taxiref.child("disponible").setValue(false);
        solicitudref.removeEventListener(this);
    }

    @Override
    public void estadoChanged(Solicitud solicitud) {

    }

    @Override
    public void onTaxistaFinishedLoading(Taxista taxista) {

    }

    @Override
    public void onUsuarioFinishedLoading(Usuario usuario, Solicitud solicitud) {
        if(solicitud.IDTAXISTA!=null && solicitud.ESTADO.equals("destino")) {
            if (solicitud.IDTAXISTA.equals(TAXI.IDTAXISTA) ) {
                sendTodasSolicitudesDeTaxista(solicitud);
            }
        }else{
            CURRENT_SOLICITUD.USUARIO = usuario;
            sendBroadcast(CURRENT_SOLICITUD);
            if(solicitud.IDTAXISTA == null && solicitud.ESTADO.equals("pendiente")){
                lanzarDialogo(solicitud);
            }else if(solicitud.IDTAXISTA!=null){
                if(solicitud.IDTAXISTA.equals(TAXISTA.ID) && solicitud.ESTADO.equals("pendiente")){
                    lanzarDialogo(solicitud);
                }
            }
        }

    }


    @Override
    public void onEmpresaFinishedLoading(Empresa empresa, Solicitud solicitud) {
        if(solicitud.IDTAXISTA.equals(TAXI.IDTAXISTA) && solicitud.ESTADO.equals("destino")){
            sendTodasSolicitudesDeTaxista(solicitud);
        }else {
            CURRENT_SOLICITUD.EMPRESA = empresa;
            sendBroadcast(CURRENT_SOLICITUD);
            if(solicitud.IDTAXISTA == null && solicitud.ESTADO.equals("pendiente")){
                lanzarDialogo(solicitud);
            }else if(solicitud.IDTAXISTA!=null){
                if(solicitud.IDTAXISTA.equals(TAXISTA.ID) && solicitud.ESTADO.equals("pendiente")){
                    lanzarDialogo(solicitud);
                }
            }
        }
    }

    @Override
    public void onTaxiFinishedLoading(Taxi taxi) {

    }
}
