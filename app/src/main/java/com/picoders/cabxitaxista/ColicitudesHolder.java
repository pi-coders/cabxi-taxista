package com.picoders.cabxitaxista;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by juancho on 4/11/16.
 */

public class ColicitudesHolder extends RecyclerView.ViewHolder  {
    ImageView imagen;
    TextView texto;
    Context context;
    Solicitud solicitud;
    View view;
    public ColicitudesHolder(View itemView) {
        super(itemView);
        imagen = (ImageView) itemView.findViewById(R.id.imagesolicitud);
        texto = (TextView) itemView.findViewById(R.id.textosolicitud);
        this.view = itemView;

    }
    public void pintarLinea(final Solicitud solicitud, final Context ctx){
        this.context=ctx;
        this.solicitud=solicitud;

            if(solicitud.USUARIO!=null)
            Picasso.with(ctx).load(solicitud.USUARIO.IMAGEN).fit().transform(new CircleTransform()).into(imagen);
            else{

                final Handler handler = new Handler();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(solicitud.USUARIO==null)
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Picasso.with(ctx).load(solicitud.USUARIO.IMAGEN).fit().transform(new CircleTransform()).into(imagen);

                            }
                        });


                    }
                }).start();

            }
            texto.setText(solicitud.DIRECCION);
       final SolicitudClickListener listener = (SolicitudClickListener)context;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSolicitudClicked(solicitud);
            }
        });



    }
    public interface SolicitudClickListener{
        void onSolicitudClicked(Solicitud solicitud);
    }
}
