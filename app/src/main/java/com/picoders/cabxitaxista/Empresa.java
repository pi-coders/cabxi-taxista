package com.picoders.cabxitaxista;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by j.hurtado on 08/03/2017.
 */

public class Empresa implements Parcelable{
    String ciudad,correo,imagen,nit,nombre,telefono;

    public Empresa (Bundle bundle){
        this.ciudad=bundle.getString("ciudad");
        this.correo=bundle.getString("correo");
        this.imagen=bundle.getString("imagen");
        this.nit=bundle.getString("nit");
        this.nombre=bundle.getString("nombre");
        this.telefono=bundle.getString("telefono");
    }

    protected Empresa(Parcel in) {
        ciudad = in.readString();
        correo = in.readString();
        imagen = in.readString();
        nit = in.readString();
        nombre = in.readString();
        telefono = in.readString();
    }

    public static final Creator<Empresa> CREATOR = new Creator<Empresa>() {
        @Override
        public Empresa createFromParcel(Parcel in) {
            return new Empresa(in);
        }

        @Override
        public Empresa[] newArray(int size) {
            return new Empresa[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ciudad);
        dest.writeString(correo);
        dest.writeString(imagen);
        dest.writeString(nit);
        dest.writeString(nombre);
        dest.writeString(telefono);
    }
}
