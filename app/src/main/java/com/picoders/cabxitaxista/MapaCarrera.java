package com.picoders.cabxitaxista;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Step;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.vision.text.Text;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.WriterException;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

import static android.R.attr.bitmap;

/**
 * Created by juancho on 19/11/16.
 */

public class MapaCarrera extends FragmentActivity implements OnMapReadyCallback,Solicitud.OnEstadoChangedListener,LocationListener, GoogleApiClient.ConnectionCallbacks {

    GoogleMap mMap;
    ImageView imageperfil;
    TextView textonombre,textodireccion,botonyalegue,botoncancelar;
    Solicitud SOLICITUD;
    Location CURRENT_LOCATION;
    MarkerOptions MARKER_TAXI;
    GoogleApiClient googleApiClient;
    Usuario USUARIO;
    Utilidades util;
    Dialog dialogoqr;
    LocationRequest locationRequest;
    List<PolylineOptions> CURRENT_RUTA=new ArrayList<>();
    Bundle bundleinicial;
    boolean firsttime=true;
    String ID_TAXISTA,IDTAXI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa_carrera);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapacarrera);
        mapFragment.getMapAsync(this);
        util = new Utilidades();
        bundleinicial=getIntent().getExtras();
        SOLICITUD = bundleinicial.getParcelable("solicitud");
        USUARIO = SOLICITUD.USUARIO;
      //  String nombreusuario = getIntent().getStringExtra("nombreUsuario");
      //  String imagenusuario = getIntent().getStringExtra("imagenUsuario");
        CURRENT_LOCATION = new Location(SOLICITUD.TAXISTA.ID);
        CURRENT_LOCATION.setLatitude(SOLICITUD.TAXISTA.CURRENT_LATITUD);
        CURRENT_LOCATION.setLongitude(SOLICITUD.TAXISTA.CURRENT_LONGITUD);

        imageperfil = (ImageView) findViewById(R.id.imageperfilusuario);
        textonombre = (TextView) findViewById(R.id.nombreusuario);
        textodireccion = (TextView) findViewById(R.id.direccioncarrera);
        botonyalegue = (TextView) findViewById(R.id.botonyallegue);
        botonyalegue.setOnClickListener(llegamosOrigenClickListener);
        botoncancelar = (TextView) findViewById(R.id.botoncancelar);
        String key=null;

        if(SOLICITUD.TIPOSOLICITUD.equals("normal")) {
            USUARIO = SOLICITUD.USUARIO;
            if(USUARIO!=null) {
                Picasso.with(MapaCarrera.this).load(USUARIO.IMAGEN).transform(new CircleTransform()).fit().into(imageperfil);
                textonombre.setText(USUARIO.NOMBRE + " " + USUARIO.APELLIDO);
                textodireccion.setText(SOLICITUD.DIRECCION);
            }else{
                SOLICITUD.obtenerUsuario(MapaCarrera.this);
            }
        }else if(SOLICITUD.TIPOSOLICITUD.equals("operadora")){
            Empresa empresa = SOLICITUD.EMPRESA;
            if(empresa!=null) {
                if (empresa.imagen.length() > 0)
                    Picasso.with(MapaCarrera.this).load(empresa.imagen).transform(new CircleTransform()).fit().into(imageperfil);
                textonombre.setText(empresa.nombre);
                textodireccion.setText(SOLICITUD.DIRECCION);
            }else {
                SOLICITUD.obtenerUsuario(MapaCarrera.this);

            }
        }

        botoncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOLICITUD.cancelarSolicitud();
            }
        });
        SOLICITUD.escucharCambiosDeEstado(MapaCarrera.this);
    }
    public void pintarRuta(){
        GoogleDirection.withServerKey(getResources().getString(R.string.google_maps_key))
                .from(MARKER_TAXI.getPosition())
                .to(SOLICITUD.MARKER.getPosition())
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        try {
                            List<Step> stepList = direction.getRouteList().get(0).getLegList().get(0).getStepList();
                            ArrayList<PolylineOptions> polylineOptionList = DirectionConverter.createTransitPolyline(MapaCarrera.this, stepList, 5, Color.CYAN, 3, Color.BLUE);
                            CURRENT_RUTA.clear();
                            CURRENT_RUTA.addAll(polylineOptionList);
                            for (PolylineOptions polylineOption : polylineOptionList) {
                                mMap.addPolyline(polylineOption);

                            }
                        }catch (IndexOutOfBoundsException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {

                    }
                });
    }
    private void updateMap(){
        mMap.clear();
        if(MARKER_TAXI!=null) {
            MARKER_TAXI.position(new LatLng(CURRENT_LOCATION.getLatitude(), CURRENT_LOCATION.getLongitude()));
            mMap.addMarker(MARKER_TAXI);
        }
        if(!SOLICITUD.ESTADO.equals("servicioInicio")) {
            mMap.addMarker(SOLICITUD.MARKER);
            for (PolylineOptions polylineOptions : CURRENT_RUTA)
                mMap.addPolyline(polylineOptions);
        }
    }
    private void retorcederAMainMap(){
        Intent intent = new Intent(this, MainMap.class);
        if(SOLICITUD.TAXISTA!=null && SOLICITUD.TAXI!=null) {
            intent.putExtra("taxi",SOLICITUD.TAXI);
            intent.putExtra("taxista",SOLICITUD.TAXISTA);
        }else{
            intent =new Intent(this,Login.class);
        }

        startActivity(intent);
        finish();
    }
    private void mostrarDialogoQr(){
        dialogoqr = new Dialog(this);
        dialogoqr.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogoqr.setContentView(R.layout.dialogo_qr);
        dialogoqr.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

//        dialogoqr.getWindow().setBackgroundDrawableResource(Color.TRANSPARENT);
        dialogoqr.setCancelable(false);

        ImageView imageusuario = (ImageView) dialogoqr.findViewById(R.id.imageusuarioqr);
        while(USUARIO==null){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Picasso.with(this).load(USUARIO.IMAGEN).transform(new CircleTransform()).fit().into(imageusuario);
        TextView nombreusuario = (TextView) dialogoqr.findViewById(R.id.nombreusuarioqr);
        nombreusuario.setText(USUARIO.NOMBRE);
        ImageView imageqr = (ImageView) dialogoqr.findViewById(R.id.codigoqr);

        // Initializing the QR Encoder with your value to be encoded, type you required and Dimension
        QRGEncoder qrgEncoder = new QRGEncoder(SOLICITUD.ID, null, QRGContents.Type.TEXT, imageqr.getWidth());
        try {
            // Getting QR-Code as Bitmap
          Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            imageqr.setImageBitmap(bitmap);
        } catch (WriterException e) {
           e.printStackTrace();
        }

        final EditText editcodigo = (EditText) dialogoqr.findViewById(R.id.editcodigoqr);
        TextView botonaceptar = (TextView) dialogoqr.findViewById(R.id.botonaceptarcodigoqr);
        botonaceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codigo = editcodigo.getText().toString();
                if(codigo==null)
                    editcodigo.setError(getResources().getString(R.string.error_edit_codigo_qr_vacio));
                else if(USUARIO.TELEFONO==null)
                    editcodigo.setError(getResources().getString(R.string.error_usuario_sin_telefono));
                else if(codigo.equals(USUARIO.TELEFONO.substring(USUARIO.TELEFONO.length()-2))){
                  //  FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID).child("estado").setValue("servicioInicio");
                    DatabaseReference solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID);
                    Map<String,Object> taskMap = new HashMap<String,Object>();
                    taskMap.put("millisServicioInicio",System.currentTimeMillis());
                    taskMap.put("estado","servicioInicio");
                    solicitudref.updateChildren(taskMap);
                    dialogoqr.dismiss();
                }else
                    editcodigo.setError(getResources().getString(R.string.error_codigo_equivocado));

            }
        });
        dialogoqr.show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
     mMap=googleMap;
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiClient.connect();
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000);


        SOLICITUD.pintarMarker(mMap);



        RelativeLayout mapLayout = (RelativeLayout)findViewById(R.id.lineamapacarrera);



        mapLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {



                //updateMap();
            }
        });




    }

    @Override
    public void estadoChanged(Solicitud solicitud) {

        if(solicitud.ESTADO.equals("origen")){
            mostrarDialogoQr();
        }else if(solicitud.ESTADO.equals("servicioInicio")){
            if (dialogoqr!=null)
                if(dialogoqr.isShowing())
                    dialogoqr.dismiss();

            botonyalegue.setText(getResources().getString(R.string.boton_llegamos_destino));
            botonyalegue.setOnClickListener(llegamosDestinoClickListener);
            updateMap();
        }else if(solicitud.ESTADO.equals("aceptado")){
            botonyalegue.setOnClickListener(llegamosOrigenClickListener);
        }else if(solicitud.ESTADO.equals("destino")){
            //TODO dialogo de destino
            DatabaseReference taxistaref = FirebaseDatabase.getInstance().getReference().child("taxistas").child(SOLICITUD.TAXISTA.ID);
            taxistaref.child("estado").setValue("libre");
            retorcederAMainMap();
        }else if(solicitud.ESTADO.contains("cancela")){
            Toast.makeText(MapaCarrera.this,getString(R.string.prompt_cancela),Toast.LENGTH_LONG).show();
            retorcederAMainMap();
        }

    }

    @Override
    public void onTaxistaFinishedLoading(Taxista taxista) {

    }

    @Override
    public void onUsuarioFinishedLoading(Usuario usuario, Solicitud solicitud) {
        USUARIO=usuario;
        Picasso.with(MapaCarrera.this).load(USUARIO.IMAGEN).transform(new CircleTransform()).fit().into(imageperfil);
        textonombre.setText(USUARIO.NOMBRE+" "+USUARIO.APELLIDO);
        textodireccion.setText(SOLICITUD.DIRECCION);

    }

    @Override
    public void onEmpresaFinishedLoading(Empresa empresa, Solicitud solicitud) {
        if (empresa.imagen.length() > 0)
            Picasso.with(MapaCarrera.this).load(empresa.imagen).transform(new CircleTransform()).fit().into(imageperfil);
        textonombre.setText(empresa.nombre);
        textodireccion.setText(SOLICITUD.DIRECCION);

    }

    @Override
    public void onTaxiFinishedLoading(Taxi taxi) {

    }

    @Override
    public void onLocationChanged(Location location) {
        CURRENT_LOCATION=location;
        if(MARKER_TAXI!=null)
            updateMap();
        else{

            Picasso.with(this).load(R.drawable.taxi).resize(80,80).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                 //   Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, 80, 80, false);
                    MARKER_TAXI = new MarkerOptions().position(new LatLng(CURRENT_LOCATION.getLatitude(), CURRENT_LOCATION.getLongitude()));
                    MARKER_TAXI.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                    mMap.addMarker(MARKER_TAXI);
                    pintarRuta();
                    updateMap();

                    LatLngBounds.Builder builder = new LatLngBounds.Builder();

                    builder.include(new LatLng(SOLICITUD.LATITUD,SOLICITUD.LONGITUD));
                    builder.include(new LatLng(CURRENT_LOCATION.getLatitude(),CURRENT_LOCATION.getLongitude()));
                    LatLngBounds bounds = builder.build();
                    int padding = 100; // offset from edges of the map in pixels
                    final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    mMap.setPadding(40,40,40,40);
                    mMap.animateCamera(cu);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });

        }
    }

    @SuppressWarnings("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    View.OnClickListener llegamosDestinoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatabaseReference solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID);
            Map<String,Object> taskMap = new HashMap<String,Object>();
            taskMap.put("destinoMillis",System.currentTimeMillis());
            taskMap.put("estado","destino");
            taskMap.put("latitud_fin",CURRENT_LOCATION.getLatitude());
            taskMap.put("longitud_fin",CURRENT_LOCATION.getLongitude());
            solicitudref.updateChildren(taskMap);

        }
    };

    View.OnClickListener llegamosOrigenClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatabaseReference solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID);
            Map<String,Object> taskMap = new HashMap<String,Object>();
            taskMap.put("millisOrigen",System.currentTimeMillis());
            if(SOLICITUD.TIPOSOLICITUD.equals("normal"))
            taskMap.put("estado","origen");
            else if(SOLICITUD.TIPOSOLICITUD.equals("operadora")){
                taskMap.put("estado","servicioInicio");
                taskMap.put("millisServicioInicio",System.currentTimeMillis());
            }
            solicitudref.updateChildren(taskMap);

            DatabaseReference taxistaref = FirebaseDatabase.getInstance().getReference().child("taxistas").child(SOLICITUD.TAXISTA.ID);
            taxistaref.child("estado").setValue("ocupado");
        }
    };

    View.OnClickListener cancelarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatabaseReference solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes").child(SOLICITUD.ID);
            Map<String,Object> taskMap = new HashMap<String,Object>();
            taskMap.put("millisCancelado",System.currentTimeMillis());
            taskMap.put("estado","cancela_taxista");
            solicitudref.updateChildren(taskMap);
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putAll(bundleinicial);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        bundleinicial=savedInstanceState;
    }
}
