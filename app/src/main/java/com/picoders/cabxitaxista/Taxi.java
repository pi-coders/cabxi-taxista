package com.picoders.cabxitaxista;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by j.hurtado on 14/10/2016.
 */

public class Taxi implements Parcelable{
    String CODIGOEMPRESA,CODIGORADIO,DISPONIBLE,IDTAXISTA,ID,MARCA,MODELO,PLACA,CIUDAD,DOCUMENTOTAXISTA,ESTADO,IMAGENTAXISTA,NOMBRETAXISTA,TELEFONOTAXISTA,NOMBREMARCA,IMAGENMARCA;
    double LATITUD,LONGITUD;
    float RUMBO;
    MarkerOptions MARKER;
    Context context;
    DatabaseReference rootref,taxiref,taxistaref;
    LatLng LATLNG;
    GoogleMap MAP;
    Bundle BUNDLE;
    boolean ISFAVORITE;

    public Taxi(){
        this.CODIGOEMPRESA=null;
        this.CODIGORADIO=null;
        this.DISPONIBLE=null;
        this.IDTAXISTA=null;
        this.ID=null;
        this.MARCA=null;
        this.MODELO=null;
        this.PLACA=null;
        this.CIUDAD=null;
        this.DOCUMENTOTAXISTA=null;
        this.ESTADO=null;
        this.IMAGENTAXISTA=null;
        this.NOMBRETAXISTA=null;
        this.TELEFONOTAXISTA=null;
        this.LATITUD=0;
        this.LONGITUD=0;
        this.RUMBO=0;
        this.MARKER=null;
        this.context=null;
        this.LATLNG=null;
        this.MAP=null;
        this.NOMBREMARCA=null;
        this.IMAGENMARCA=null;
        this.BUNDLE=null;
        this.ISFAVORITE=false;
    }

    public Taxi(Context ctx, Bundle bundle, final GoogleMap map){
        this.BUNDLE=bundle;
        this.CODIGOEMPRESA=bundle.getString("codigoEmpresa");
        this.CODIGORADIO=bundle.getString("codigoRadio");
        this.DISPONIBLE=bundle.getString("disponible");
        this.IDTAXISTA=bundle.getString("idTaxista");
        this.ID=bundle.getString("idTaxi");
        this.MARCA=bundle.getString("marca");
        this.MODELO=bundle.getString("modelo");
        this.PLACA=bundle.getString("placa");
        this.CIUDAD=bundle.getString("ciudad");
        this.DOCUMENTOTAXISTA=bundle.getString("documento");
        this.ESTADO=bundle.getString("estado");
        this.IMAGENTAXISTA=bundle.getString("imagen");
        this.NOMBRETAXISTA=bundle.getString("nombre");
        this.TELEFONOTAXISTA=bundle.getString("telefono");

        this.LATLNG=new LatLng(LATITUD,LONGITUD);
        try {
            this.LATITUD=Double.parseDouble(bundle.getString("latitud"));
            this.LONGITUD=Double.parseDouble(bundle.getString("longitud"));
            this.RUMBO = Float.parseFloat(bundle.getString("rumbo"));
        }catch (NullPointerException e){

        }
        try{
            this.IMAGENMARCA = bundle.getString("imagenMarca");
            this.NOMBREMARCA = bundle.getString("nombreMarca");
            this.MAP=map;
        }catch (NullPointerException e){

        }


        this.context=ctx;
        this.rootref= FirebaseDatabase.getInstance().getReference();
     //   BitmapDrawable bitmapdraw=(BitmapDrawable) context.getResources().getDrawable(R.drawable.taxi);
        final Taxi objectctx = this;

        Picasso.with(ctx).load(R.drawable.icontaxi).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, 50, 70, false);
                MARKER=new MarkerOptions().position(LATLNG).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)).anchor(25,35).rotation(RUMBO);
               if(map!=null)
                map.addMarker(MARKER);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });


if(MARCA!=null && IMAGENMARCA==null)
        rootref.child("marcas").child(MARCA).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    NOMBREMARCA = dataSnapshot.child("nombre").getValue().toString();
                    IMAGENMARCA = dataSnapshot.child("logo").getValue().toString();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setMap(GoogleMap map){
        this.MAP = map;
        if(MARKER!=null)
        map.addMarker(MARKER);
    }

    protected Taxi(Parcel in) {
        CODIGOEMPRESA = in.readString();
        CODIGORADIO = in.readString();
        DISPONIBLE = in.readString();
        IDTAXISTA = in.readString();
        ID = in.readString();
        MARCA = in.readString();
        MODELO = in.readString();
        PLACA = in.readString();
        CIUDAD = in.readString();
        DOCUMENTOTAXISTA = in.readString();
        ESTADO = in.readString();
        IMAGENTAXISTA = in.readString();
        NOMBRETAXISTA = in.readString();
        TELEFONOTAXISTA = in.readString();
        NOMBREMARCA = in.readString();
        IMAGENMARCA = in.readString();
        LATITUD = in.readDouble();
        LONGITUD = in.readDouble();
        RUMBO = in.readFloat();
        MARKER = in.readParcelable(MarkerOptions.class.getClassLoader());
        LATLNG = in.readParcelable(LatLng.class.getClassLoader());
        BUNDLE = in.readBundle();
        ISFAVORITE = in.readByte() != 0;
    }

    public static final Creator<Taxi> CREATOR = new Creator<Taxi>() {
        @Override
        public Taxi createFromParcel(Parcel in) {
            return new Taxi(in);
        }

        @Override
        public Taxi[] newArray(int size) {
            return new Taxi[size];
        }
    };

    public void siEsFavorito(){
        this.ISFAVORITE=true;
    }
    public void noEsFavorito(){
        this.ISFAVORITE=false;
    }


    public void escucharMovimiento(){
         taxiref = rootref.child("taxis").child(ID);
        taxiref.addValueEventListener(movimientoListener);

    }
    public void noEscucharMovimiento(){
        taxiref.removeEventListener(movimientoListener);
    }
    public void escucharEstado(){
        taxistaref=rootref.child("taxistas").child(IDTAXISTA);
        taxistaref.addValueEventListener(estadoListener);
    }
    public void noEscucharEstado(){
        taxistaref.removeEventListener(estadoListener);
    }

    private ValueEventListener estadoListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            ESTADO=dataSnapshot.child("estado").getValue().toString();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private ValueEventListener movimientoListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Bundle positionbundle = new Bundle();
            for(DataSnapshot snap:dataSnapshot.getChildren()){
                positionbundle.putString(snap.getKey(),snap.getValue().toString());
            }
            positionbundle.putString("idTaxi",dataSnapshot.getKey());

            LATITUD=Double.parseDouble(positionbundle.getString("latitud"));
            LONGITUD=Double.parseDouble(positionbundle.getString("longitud"));
            RUMBO = Float.parseFloat(positionbundle.getString("rumbo"));
            LATLNG= new LatLng(LATITUD,LONGITUD);
          //  MARKER.position(LATLNG);
            if(MARKER!=null) {
                MARKER.rotation(RUMBO);
                if (MAP != null)
                    animateMarker(MARKER, LATLNG, false);
            }
            Intent intentoposicion = new Intent("movimiento");
            intentoposicion.putExtras(positionbundle);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intentoposicion);


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };


    private void animateMarker(final MarkerOptions marker, final LatLng toPosition,
                               final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();

        Projection proj = MAP.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.position(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                    //    marker.(false);
                    } else {
                     //   marker.setVisible(true);
                    }
                }
            }
        });
    }
    public void cambiarPosicion(Location location){
        DatabaseReference taxisref = FirebaseDatabase.getInstance().getReference().child("taxis").child(ID);
       // taxistaref.child("rumbo").setValue(location.getBearing());
        Map<String, Object> data= new HashMap<String, Object>();
        data.put("latitud",location.getLatitude());
        data.put("longitud",location.getLongitude());
        data.put("rumbo",location.getBearing());
        taxisref.updateChildren(data);
        //  data.put("rumbo",location.getBearing());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CODIGOEMPRESA);
        dest.writeString(CODIGORADIO);
        dest.writeString(DISPONIBLE);
        dest.writeString(IDTAXISTA);
        dest.writeString(ID);
        dest.writeString(MARCA);
        dest.writeString(MODELO);
        dest.writeString(PLACA);
        dest.writeString(CIUDAD);
        dest.writeString(DOCUMENTOTAXISTA);
        dest.writeString(ESTADO);
        dest.writeString(IMAGENTAXISTA);
        dest.writeString(NOMBRETAXISTA);
        dest.writeString(TELEFONOTAXISTA);
        dest.writeString(NOMBREMARCA);
        dest.writeString(IMAGENMARCA);
        dest.writeDouble(LATITUD);
        dest.writeDouble(LONGITUD);
        dest.writeFloat(RUMBO);
        dest.writeParcelable(MARKER, flags);
        dest.writeParcelable(LATLNG, flags);
        dest.writeBundle(BUNDLE);
        dest.writeByte((byte) (ISFAVORITE ? 1 : 0));
    }
}
