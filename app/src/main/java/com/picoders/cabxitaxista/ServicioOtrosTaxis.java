package com.picoders.cabxitaxista;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by j.hurtado on 14/10/2016.
 */

public class ServicioOtrosTaxis extends Service{
    String CIUDAD;
    DatabaseReference rootref;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            CIUDAD = intent.getStringExtra("ciudad");
        }catch (NullPointerException e){
            CIUDAD="Bogotá";
        }
        rootref= FirebaseDatabase.getInstance().getReference();

        final Query querytaxis = rootref.child("taxis").orderByChild("disponible").equalTo(true);
        querytaxis.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final Bundle bundletaxis=new Bundle();
                for(DataSnapshot snap : dataSnapshot.getChildren()){
                    bundletaxis.putString(snap.getKey(),snap.getValue().toString());
                }
                bundletaxis.putString("idTaxi",dataSnapshot.getKey());



                 try {
                     DatabaseReference querytaxistas = rootref.child("taxistas").child(dataSnapshot.child("idTaxista").getValue().toString());

                     querytaxistas.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Bundle bundletaxista = new Bundle();
                            for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                bundletaxista.putString(snap.getKey(), snap.getValue().toString());
                            }
                            bundletaxista.putString("idTaxista",dataSnapshot.getKey());

                            bundletaxis.putAll(bundletaxista);

                            try {
                              //todo filtrar los taxis para no tener que traer todos los de la base de datos que tengan disponible:true
                              //  if (bundletaxis.getString("ciudad").equals(CIUDAD)) {

                                    Intent intentotaxi = new Intent("nuevotaxi");

                                    intentotaxi.putExtras(bundletaxis);
                                    LocalBroadcastManager.getInstance(ServicioOtrosTaxis.this).sendBroadcast(intentotaxi);

                              //  }
                            }catch (NullPointerException e){

                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }catch (NullPointerException e){
//                    e.printStackTrace();
                }


                /*
                String codigoEmpresa = dataSnapshot.child("codigoEmpresa").getValue().toString();
                String codigoradio =dataSnapshot.child("codigoRadio").getValue().toString();
                String disponible = dataSnapshot.child("disponible").getValue().toString();
                String idTaxista=dataSnapshot.child("idTaxista").getValue().toString();
                String idTaxi = dataSnapshot.getKey();
                String latitud = dataSnapshot.child("latitud").getValue().toString();
                String longitud = dataSnapshot.child("longitud").getValue().toString();
                String rumbo = dataSnapshot.child("tumbo").getValue().toString();
                String marca= dataSnapshot.child("marca").getValue().toString();
                String modelo = dataSnapshot.child("modelo").getValue().toString();
                String placa = dataSnapshot.child("placa").getValue().toString();
                */
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        /*        String id = dataSnapshot.getKey();
                String latitud = dataSnapshot.child("latitud").getValue().toString();
                String longitud = dataSnapshot.child("longitud").getValue().toString();
                String rumbo = dataSnapshot.child("rumbo").getValue().toString();
                Intent intentotaxi = new Intent("movimientotaxi");
                intentotaxi.putExtra("idTaxi",id);
                intentotaxi.putExtra("latitud",latitud);
                intentotaxi.putExtra("longitud",longitud);
                intentotaxi.putExtra("rumbo",rumbo);
                LocalBroadcastManager.getInstance(ServicioOtrosTaxis.this).sendBroadcast(intentotaxi);*/
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        return super.onStartCommand(intent, flags, startId);
    }
}
