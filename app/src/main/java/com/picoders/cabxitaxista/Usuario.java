package com.picoders.cabxitaxista;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by juancho on 18/11/16.
 */

public class Usuario implements Parcelable{
    String APELLIDO,NOMBRE,CORREO,IMAGEN,ID,TELEFONO;
    public Usuario(Bundle bundle){
        this.APELLIDO=bundle.getString("apellidos");
        this.NOMBRE = bundle.getString("nombres");
        this.CORREO = bundle.getString("correo");
        this.IMAGEN = bundle.getString("imagen");
        this.ID = bundle.getString("idUsuario");
        this.TELEFONO = bundle.getString("telefono");
    }

    protected Usuario(Parcel in) {
        APELLIDO = in.readString();
        NOMBRE = in.readString();
        CORREO = in.readString();
        IMAGEN = in.readString();
        ID = in.readString();
        TELEFONO = in.readString();
    }

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(APELLIDO);
        dest.writeString(NOMBRE);
        dest.writeString(CORREO);
        dest.writeString(IMAGEN);
        dest.writeString(ID);
        dest.writeString(TELEFONO);
    }
}
