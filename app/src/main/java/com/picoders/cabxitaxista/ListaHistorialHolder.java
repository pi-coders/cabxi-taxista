package com.picoders.cabxitaxista;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by juancho on 4/12/16.
 */

public class ListaHistorialHolder extends RecyclerView.ViewHolder {

    ImageView imagen;
    TextView nombre, fecha,direccion;
    Context context;
    Solicitud solicitud;
    Utilidades utilidades;
    public ListaHistorialHolder(View itemView) {
        super(itemView);

        imagen = (ImageView) itemView.findViewById(R.id.historial_imagen);
        nombre = (TextView) itemView.findViewById(R.id.historial_nombre);
        fecha = (TextView) itemView.findViewById(R.id.historial_fecha);
        direccion = (TextView) itemView.findViewById(R.id.historial_direccion);
    }
    public void pintarLinea(Context context,Solicitud solicitud){
        this.context=context;
        this.solicitud=solicitud;
        utilidades=new Utilidades();
        if(solicitud.TIPOSOLICITUD.equals("normal")) {
            nombre.setText(solicitud.USUARIO.NOMBRE);
            if(solicitud.USUARIO.IMAGEN.length()>0)
            Picasso.with(context).load(solicitud.USUARIO.IMAGEN).transform(new CircleTransform()).fit().into(imagen);
            else
                Picasso.with(context).load(R.drawable.perfil).transform(new CircleTransform()).fit().into(imagen);
        }else if(solicitud.equals("operadora")){
            nombre.setText(solicitud.EMPRESA.nombre);
            if(solicitud.EMPRESA.imagen.length()>0)
            Picasso.with(context).load(solicitud.EMPRESA.imagen).transform(new CircleTransform()).fit().into(imagen);
            else
                Picasso.with(context).load(R.drawable.logo).transform(new CircleTransform()).fit().into(imagen);
        }

        if(solicitud.MILLISPEDIDO!=null)
        fecha.setText(utilidades.convertDate(solicitud.MILLISPEDIDO,context.getResources().getString(R.string.formato_fechas)));
        direccion.setText(solicitud.DIRECCION);
    }
}
