package com.picoders.cabxitaxista;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;

/**
 * Created by juancho on 15/10/16.
 */

public class Solicitud implements Parcelable {
    String CIUDAD,DIRECCION,IDUSUARIO,TIPOSOLICITUD,IDTAXISTA,IDTAXI,ID,DIRECCIONDESTINO,ESTADO;
    Long MILLISPEDIDO,MILLISDESTINO;
    double LATITUD,LONGITUD;
    DatabaseReference SOLICITUDREF;
    Bundle BUNDLE;
    Taxista TAXISTA;
    Taxi TAXI;
    Context context;
    int CALIFICACION;
    String MARCA;
    boolean BAUL;
    Usuario USUARIO;
    Empresa EMPRESA;
    OnEstadoChangedListener listener;
    MarkerOptions MARKER;


    public Solicitud(){
        this.CIUDAD=null;
        this.DIRECCION=null;
        this.IDUSUARIO=null;
        this.TIPOSOLICITUD=null;
        this.MILLISPEDIDO=null;
        this.IDTAXI=null;
        this.IDTAXISTA=null;
        this.LATITUD=0;
        this.LONGITUD=0;
        this.ID=null;
        this.BUNDLE=null;
        this.TAXISTA=null;
        this.TAXI=null;
        this.MILLISDESTINO=null;
        this.CALIFICACION=0;
        this.MARCA=null;
        this.BAUL=false;
        this.ESTADO=null;
        this.MARKER=null;

    }
    public Solicitud(final Context context, final Bundle bundle){
        this.context=context;
        this.BUNDLE=bundle;
        this.CIUDAD=bundle.getString("ciudad");
        this.DIRECCION=bundle.getString("direccion");
        this.IDUSUARIO=bundle.getString("idUsuario");
        this.ESTADO = bundle.getString("estado");
        this.TIPOSOLICITUD=bundle.getString("tipoSolicitud");
        this.listener = (OnEstadoChangedListener) context;
        try {
            this.MILLISPEDIDO = (long)Double.parseDouble(bundle.getString("millisPedido"));
        }catch (NumberFormatException e){
            //e.printStackTrace();
        }catch (NullPointerException e){

        }
        try{
            this.MILLISDESTINO=Long.parseLong(bundle.getString("destinoMillis"));
        }catch (NumberFormatException e){
         //   e.printStackTrace();
        }catch (NullPointerException e){
            // e.printStackTrace();
        }
        try {
            this.LATITUD = Double.parseDouble(bundle.getString("latitud"));
            this.LONGITUD = Double.parseDouble(bundle.getString("longitud"));
        }catch (NullPointerException e){
            //   e.printStackTrace();
        }
        try{
            this.CALIFICACION = Integer.parseInt(bundle.getString("calificacion"));
        }catch (NumberFormatException e){
            //      e.printStackTrace();
        }catch (NullPointerException e){
            //    e.printStackTrace();
        }

        this.IDTAXI=bundle.getString("idTaxi");
        this.IDTAXISTA=bundle.getString("idTaxista");
        this.ID=bundle.getString("idSolicitud");
        this.MARCA=bundle.getString("marca");
        try {
            this.BAUL = Boolean.parseBoolean(bundle.getString("baul"));
        }catch (Exception e){
            e.printStackTrace();
        }





        //obtenerUsuario(context);


    }
    public void obtenerTaxi(final Context contexto){
        if(listener==null)
            this.listener = (OnEstadoChangedListener) contexto;

        try{
            FirebaseDatabase.getInstance().getReference().child("taxis").child(IDTAXI).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Bundle bundle2 = new Bundle();
                    for(DataSnapshot snap: dataSnapshot.getChildren())
                        bundle2.putString(snap.getKey(),snap.getValue().toString());

                    bundle2.putString("idTaxi",IDTAXI);

                    TAXI=new Taxi(contexto,bundle2,null);
                    listener.onTaxiFinishedLoading(TAXI);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public void obtenerTaxista(Context contexto){

        if(listener==null)
            this.listener = (OnEstadoChangedListener) contexto;

        try {
            FirebaseDatabase.getInstance().getReference().child("taxistas").child(IDTAXISTA).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Bundle bundle1 = new Bundle();
                    for (DataSnapshot snap : dataSnapshot.getChildren())
                        bundle1.putString(snap.getKey(), snap.getValue().toString());

                    bundle1.putString("idTaxista",IDTAXISTA);
                    TAXISTA = new Taxista(context,bundle1);
                    OnEstadoChangedListener listener = (OnEstadoChangedListener) context;
                    listener.onTaxistaFinishedLoading(TAXISTA);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }
    public void obtenerUsuario(Context contexto){
        if(listener==null)
            this.listener = (OnEstadoChangedListener) contexto;
        String key=null;
        if(TIPOSOLICITUD.equals("normal"))
            key="usuarios";
        else if(TIPOSOLICITUD.equals("operadora"))
            key="empresas";
        try{
            final String finalkey=key;
            FirebaseDatabase.getInstance().getReference().child(key).child(IDUSUARIO).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Bundle bundleusuario = new Bundle();
                    for(DataSnapshot snap: dataSnapshot.getChildren())
                        bundleusuario.putString(snap.getKey(),snap.getValue().toString());
                    if(finalkey.equals("usuarios")) {
                        bundleusuario.putString("idUsuario", dataSnapshot.getKey());
                        USUARIO = new Usuario(bundleusuario);
                        listener.onUsuarioFinishedLoading(USUARIO,Solicitud.this);
                    }else if(finalkey.equals("empresas")){
                        bundleusuario.putString("idEmpresa",dataSnapshot.getKey());
                        EMPRESA = new Empresa(bundleusuario);
                        listener.onEmpresaFinishedLoading(EMPRESA, Solicitud.this);

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    protected Solicitud(Parcel in) {
        CIUDAD = in.readString();
        DIRECCION = in.readString();
        IDUSUARIO = in.readString();
        TIPOSOLICITUD = in.readString();
        IDTAXISTA = in.readString();
        IDTAXI = in.readString();
        ID = in.readString();
        DIRECCIONDESTINO = in.readString();
        ESTADO = in.readString();
        LATITUD = in.readDouble();
        LONGITUD = in.readDouble();
        BUNDLE = in.readBundle();
        TAXISTA = in.readParcelable(Taxista.class.getClassLoader());
        TAXI = in.readParcelable(Taxi.class.getClassLoader());
        CALIFICACION = in.readInt();
        MARCA = in.readString();
        BAUL = in.readByte() != 0;
        USUARIO = in.readParcelable(Usuario.class.getClassLoader());
        EMPRESA = in.readParcelable(Empresa.class.getClassLoader());
        MARKER = in.readParcelable(MarkerOptions.class.getClassLoader());
    }

    public static final Creator<Solicitud> CREATOR = new Creator<Solicitud>() {
        @Override
        public Solicitud createFromParcel(Parcel in) {
            return new Solicitud(in);
        }

        @Override
        public Solicitud[] newArray(int size) {
            return new Solicitud[size];
        }
    };

    public void lanzarFirebase(){
        this.SOLICITUDREF= FirebaseDatabase.getInstance().getReference().child("solicitudes");
    }
    public String lanzarSolicitud(){
        String resultado=null;
        if(SOLICITUDREF==null)
            lanzarFirebase();
        String key=SOLICITUDREF.push().getKey();
        SOLICITUDREF.child(key).child("ciudad").setValue(CIUDAD);
        SOLICITUDREF.child(key).child("direccion").setValue(DIRECCION);
        SOLICITUDREF.child(key).child("millisPedido").setValue(MILLISPEDIDO);
        SOLICITUDREF.child(key).child("idUsuario").setValue(IDUSUARIO);
        SOLICITUDREF.child(key).child("latitud").setValue(LATITUD);
        SOLICITUDREF.child(key).child("longitud").setValue(LONGITUD);
        SOLICITUDREF.child(key).child("tipoSolicitud").setValue("normal");
        SOLICITUDREF.child(key).child("estado").setValue("pendiente");
        SOLICITUDREF.child(key).child("marca").setValue(MARCA);

        SOLICITUDREF.child(key).child("baul").setValue(BAUL);
        if(IDTAXISTA!=null)
            SOLICITUDREF.child(key).child("idTaxista").setValue(IDTAXISTA);
        this.ID=key;
        resultado=key;

        return resultado;
    }
    public Bundle getInfo(){
        BUNDLE.putString("idSolicitud",ID);
        return BUNDLE;
    }
    public void eliminarSolicitud(){
        FirebaseDatabase.getInstance().getReference().child("solicitudes").child(ID).removeValue();
    }
    public void setListener(Context context){
        this.listener = (OnEstadoChangedListener) context;
    }
    public void cambiarEstado(String estado,Context contexto){
       // OnEstadoChangedListener listener = (OnEstadoChangedListener) context;
        if(listener==null)
            this.listener = (OnEstadoChangedListener) contexto;
        BUNDLE.putString("estado",estado);
        this.ESTADO=estado;
        listener.estadoChanged(this);
    }
    public void escucharCambiosDeEstado(final Context contexto){
        if(SOLICITUDREF==null)
            lanzarFirebase();

        SOLICITUDREF.child(ID).child("estado").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null) {
                    ESTADO = dataSnapshot.getValue().toString();
                    cambiarEstado(dataSnapshot.getValue().toString(),contexto);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void pintarMarker(final GoogleMap map){
        this.MARKER = new MarkerOptions().position(new LatLng(LATITUD, LONGITUD));
        this.MARKER.title(DIRECCION);
        Picasso.with(context).load(R.drawable.llamador).resize(80,80).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                MARKER.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                map.addMarker(MARKER);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });


    }
    public void cancelarSolicitud(){
        lanzarFirebase();
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("estado","cancelado_taxista");
        hashMap.put("millisCancelado",String.valueOf(System.currentTimeMillis()));
        SOLICITUDREF.child(ID).updateChildren(hashMap,null);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CIUDAD);
        dest.writeString(DIRECCION);
        dest.writeString(IDUSUARIO);
        dest.writeString(TIPOSOLICITUD);
        dest.writeString(IDTAXISTA);
        dest.writeString(IDTAXI);
        dest.writeString(ID);
        dest.writeString(DIRECCIONDESTINO);
        dest.writeString(ESTADO);
        dest.writeDouble(LATITUD);
        dest.writeDouble(LONGITUD);
        dest.writeBundle(BUNDLE);
        dest.writeParcelable(TAXISTA, flags);
        dest.writeParcelable(TAXI, flags);
        dest.writeInt(CALIFICACION);
        dest.writeString(MARCA);
        dest.writeByte((byte) (BAUL ? 1 : 0));
        dest.writeParcelable(USUARIO, flags);
        dest.writeParcelable(EMPRESA, flags);
        dest.writeParcelable(MARKER, flags);
    }

    public interface OnEstadoChangedListener{
        void estadoChanged(Solicitud solicitud);
        void onTaxistaFinishedLoading(Taxista taxista);
        void onUsuarioFinishedLoading(Usuario usuario, Solicitud solicitud);
        void onEmpresaFinishedLoading(Empresa empresa, Solicitud solicitud);
        void onTaxiFinishedLoading(Taxi taxi);
    }

}
