package com.picoders.cabxitaxista;

import android.*;
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainMap extends AppCompatActivity implements OnMapReadyCallback,Solicitud.OnEstadoChangedListener, GoogleApiClient.ConnectionCallbacks,LocationListener,Taxista.onTaxistaListener,ColicitudesHolder.SolicitudClickListener {

    private GoogleMap mMap;
    private static int ZOOM_LEVEL=14;
    Intent intentoOtrosTaxis;
    Intent intentoSolicitudes;
    Intent intentoReporte;
    Taxista TAXISTA;
    Taxi TAXI;
    List<Taxi> LISTA_TAXIS;
    List<Solicitud> LISTA_SOLICITUDES;
    List<Solicitud> LISTA_HISTORIAL;
    TextView botonestado;
    RecyclerView lineasolicitudes,recyclerhistorial;
    SolicitudesAdapter solicitudesAdapter;
    ListaHistorialAdapter historialAdapter;
    GoogleApiClient googleApiClient;
    LocationRequest locationRequest;
    int DISTACIA_DESCUBRIMIENTO_SOLICITUDES=7000;
    RelativeLayout lineamapa;
    LinearLayout lineaperfil,lineahistorial;
    Bundle bundleinicial;
    SessionDB sessionDB;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
         mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sessionDB = new SessionDB(this,"session",null,1);

        bundleinicial = getIntent().getExtras();
        TAXISTA = bundleinicial.getParcelable("taxista");
        TAXISTA.CURRENT_LONGITUD=0;
        TAXI = bundleinicial.getParcelable("taxi");

        lineamapa = (RelativeLayout) findViewById(R.id.lineamapa);
        lineaperfil = (LinearLayout) findViewById(R.id.lineaperfil);
        lineahistorial = (LinearLayout) findViewById(R.id.lineahistorial);

        intentoOtrosTaxis = new Intent(this,ServicioOtrosTaxis.class);
        intentoSolicitudes = new Intent(this,ServicioSolicitudes.class);
        intentoReporte = new Intent(this,ServicioReportePosicion.class);
        intentoOtrosTaxis.putExtra("ciudad",TAXISTA.CIUDAD);

        intentoReporte.putExtra("taxista",TAXISTA);
        intentoReporte.putExtra("taxi",TAXI);


        LISTA_TAXIS=new ArrayList<>();
        LISTA_SOLICITUDES=new ArrayList<>();
        LISTA_HISTORIAL = new ArrayList<>();
        botonestado = (TextView) findViewById(R.id.botonestado);
        lineasolicitudes = (RecyclerView) findViewById(R.id.lineasolicitudes);
        recyclerhistorial = (RecyclerView) findViewById(R.id.recyclerhistorial);
        lineasolicitudes.setLayoutManager(new LinearLayoutManager(this));
        recyclerhistorial.setLayoutManager(new LinearLayoutManager(this,LinearLayout.VERTICAL,true));
        solicitudesAdapter = new SolicitudesAdapter(this,LISTA_SOLICITUDES);
        historialAdapter = new ListaHistorialAdapter(this,LISTA_HISTORIAL);
        lineasolicitudes.setAdapter(solicitudesAdapter);
        recyclerhistorial.setAdapter(historialAdapter);

        botonestado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TAXISTA.ESTADO!=null)
                if(TAXISTA.ESTADO.equals("libre")){
                    TAXISTA.cambiarEstado("ocupado");
                }else if(TAXISTA.ESTADO.equals("ocupado") || TAXISTA.ESTADO.equals("asignado")){
                    TAXISTA.cambiarEstado("libre");
                }
            }
        });

        TAXISTA.escucharCambiosEstado(this);
        TAXISTA.cambiarEstado("libre");

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        TAXI.setMap(googleMap);
        intentoSolicitudes.putExtras(bundleinicial);
        intentoSolicitudes.putExtra("ciudad",TAXISTA.CIUDAD);
        intentoSolicitudes.putExtra("distancia_descubrir",DISTACIA_DESCUBRIMIENTO_SOLICITUDES);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // You need to ask the user to enable the permissions

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

        } else {
           lanzarMapa();
        }




    }
    @SuppressWarnings("MissingPermission")
    private void lanzarMapa(){
        mMap.setMyLocationEnabled(true);


        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
        googleApiClient.connect();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(5000); // Update location every 5 second
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            showGPSDisabledAlertToUser();

        pintarPerfil();
        //obtenerHistorial();
        try {
            Thread.sleep(750);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        startService(intentoSolicitudes);
        startService(intentoReporte);
    }
    public void actualizarMapa() {
        mMap.clear();
        for (Taxi taxi : LISTA_TAXIS)
            if (taxi.MARKER != null)
                mMap.addMarker(taxi.MARKER);
       /* if(TAXISTA!=null)
                if(TAXISTA.MARKER!=null)
                    mMap.addMarker(TAXISTA.MARKER);*/

    }
    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void pintarPerfil(){
        ImageView imageperfil = (ImageView) findViewById(R.id.perfil_image);
        TextView nombreperfil = (TextView) findViewById(R.id.perfil_nombre);
        TextView cedulaperfil = (TextView) findViewById(R.id.perfil_cedula);
        TextView placaperfil = (TextView) findViewById(R.id.perfil_placa);
        TextView telefonoperfil = (TextView) findViewById(R.id.perfil_telefono);

        Picasso.with(this).load(TAXISTA.IMAGEN).fit().into(imageperfil);
        nombreperfil.setText(TAXISTA.NOMBRE);
        cedulaperfil.setText(TAXISTA.DOCUMENTO);
        placaperfil.setText(TAXI.PLACA);
        telefonoperfil.setText(TAXISTA.TELEFONO);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    lanzarMapa();
                }
        }

    }

    BroadcastReceiver TodasSolicitudesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Solicitud solicitud = intent.getParcelableExtra("solicitud");
            LISTA_HISTORIAL.add(solicitud);
            historialAdapter.notifyDataSetChanged();
        }
    };
    BroadcastReceiver OtrosTaxisReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Taxi taxi = new Taxi(MainMap.this,intent.getExtras(),mMap);
            LISTA_TAXIS.add(taxi);
            if(mMap!=null)
            actualizarMapa();

        }
    };
    BroadcastReceiver NuevaSolicitudReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Solicitud solicitud = intent.getParcelableExtra("solicitud");
            if(solicitud.ESTADO.equals("pendiente")) {
                boolean isthere=false;
                for(Solicitud sol :LISTA_SOLICITUDES){
                    if(sol.DIRECCION.equals(solicitud.DIRECCION))
                        isthere=true;
                }
                if(!isthere) {
                    LISTA_SOLICITUDES.add(solicitud);
                    solicitud.setListener(MainMap.this);
                    solicitud.escucharCambiosDeEstado(MainMap.this);
                    solicitudesAdapter.notifyItemInserted(LISTA_SOLICITUDES.size());
                }
            }else if(!solicitud.ESTADO.equals("destino") && !solicitud.ESTADO.contains("cancela")){
               // Solicitud newsol = new Solicitud(MainMap.this,solicitud.BUNDLE);
                lanzarMapaCarrera(solicitud);
            }



        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(OtrosTaxisReceiver,
                new IntentFilter("nuevotaxi"));
        LocalBroadcastManager.getInstance(this).registerReceiver(NuevaSolicitudReceiver,
                new IntentFilter("nuevasolicitud"));
        LocalBroadcastManager.getInstance(this).registerReceiver(TodasSolicitudesReceiver,
                new IntentFilter("todassolicitudes"));
        startService(intentoOtrosTaxis);
   //     startService(intentoSolicitudes);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(OtrosTaxisReceiver);
        stopService(intentoOtrosTaxis);

    }

    @Override
    public void estadoChanged(Solicitud solicitud) {
        int index = 0;
        for (int i=0;i<LISTA_SOLICITUDES.size();i++)
            if(LISTA_SOLICITUDES.get(i).ID.equals(solicitud.ID))
                index=i;
        if(solicitud.ESTADO.equals("pendiente")){

        }
        else{
            try {
                LISTA_SOLICITUDES.remove(index);
                solicitudesAdapter.notifyItemRemoved(index);
                if (!solicitud.ESTADO.contains("cancela")) {
                    //TODO Lanzar mapa carrera
                    lanzarMapaCarrera(solicitud);
                }
            }catch (ArrayIndexOutOfBoundsException e){
               // e.printStackTrace();
            }catch (IndexOutOfBoundsException e){

            }
        }


    }
    private void lanzarMapaCarrera(Solicitud SOLICITUD){
        Intent intentomapacarrera = new Intent(this,MapaCarrera.class);
        //SOLICITUD.obtenerUsuario(MainMap.this);
      //  while(SOLICITUD.USUARIO==null){}
        SOLICITUD.TAXISTA =TAXISTA;
        SOLICITUD.TAXI = TAXI;
        intentomapacarrera.putExtra("solicitud",SOLICITUD);

       //
     //   intentomapacarrera.putExtra("nombreUsuario",SOLICITUD.USUARIO.NOMBRE+" "+SOLICITUD.USUARIO.APELLIDO);
     //   intentomapacarrera.putExtra("imagenUsuario",SOLICITUD.USUARIO.IMAGEN);
        intentomapacarrera.putExtra("currentLatitud",TAXISTA.CURRENT_LATITUD);
        intentomapacarrera.putExtra("currentLongitud",TAXISTA.CURRENT_LONGITUD);
        startActivity(intentomapacarrera);
        stopService(intentoSolicitudes);
        finish();
    }

    private void lanzarDialogoSalir(){

        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setButtonsColorRes(R.color.colorCancelar)
                .setIcon(R.drawable.exit)
                .setTitle(R.string.titulo_dialogo_salir)
                .setMessage(R.string.promt_salir)
                .setPositiveButton(R.string.salir, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseReference taxiref=FirebaseDatabase.getInstance().getReference().child("taxis").child(TAXI.ID);
                        taxiref.child("idTaxista").setValue(null);
                        sessionDB.borrarTabla();
                        stopService(intentoSolicitudes);
                        stopService(intentoReporte);
                        finish();
                    }
                })
                .setNegativeButton(R.string.prompt_cancelar, null)
                .show();
    }

    @Override
    public void onTaxistaFinishedLoading(Taxista taxista) {

    }

    @Override
    public void onUsuarioFinishedLoading(Usuario usuario, Solicitud solicitud) {

    }

    @Override
    public void onEmpresaFinishedLoading(Empresa empresa, Solicitud solicitud) {

    }

    @Override
    public void onTaxiFinishedLoading(Taxi taxi) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(TAXISTA.CURRENT_LONGITUD==0) {

            TAXISTA.setLatLng(location.getLatitude(),location.getLongitude(),MainMap.this);
            CameraPosition position = new CameraPosition.Builder()
                    .target(TAXISTA.MARKER.getPosition())
                    .zoom(ZOOM_LEVEL).build();

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }
        TAXISTA.setLatLng(location.getLatitude(),location.getLongitude(),MainMap.this);
        actualizarMapa();

        //mMap.moveCamera(CameraUpdateFactory.newLatLng(TAXISTA.MARKER.getPosition()));
    }

    @Override
    public void onMarkerCreated() {
       // mMap.addMarker(TAXISTA.MARKER);

    }

    @Override
    public void onTaxistaEstadoChanged(String estado) {
        if(estado.equals("libre")){
            stopService(intentoSolicitudes);
            startService(intentoSolicitudes);
            botonestado.setBackgroundResource(R.drawable.boton_aceptar);
            botonestado.setPadding(15,15,15,15);
            botonestado.setText(getResources().getString(R.string.estado_libre));
        }else if(estado.equals("ocupado") || estado.equals("asignado")){
            botonestado.setBackgroundResource(R.drawable.boton_cancelar);
            botonestado.setText(getResources().getString(R.string.estado_ocupado));
            botonestado.setPadding(15,15,15,15);
            stopService(intentoSolicitudes);
            LISTA_SOLICITUDES.clear();
            solicitudesAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onSolicitudClicked(Solicitud solicitud) {

       if(solicitud.ESTADO.equals("pendiente")) {
           DatabaseReference solicitudref = FirebaseDatabase.getInstance().getReference().child("solicitudes").child(solicitud.ID);
           Map<String, Object> taskMap = new HashMap<String, Object>();
           taskMap.put("idTaxi", TAXI.ID);
           taskMap.put("idTaxista", TAXISTA.ID);
           taskMap.put("millisAceptado", System.currentTimeMillis());
           taskMap.put("estado", "aceptado");
           solicitud.IDTAXISTA=TAXISTA.ID;
           solicitud.IDTAXI=TAXI.ID;
           solicitudref.updateChildren(taskMap);

           DatabaseReference taxistaref = FirebaseDatabase.getInstance().getReference().child("taxistas").child(TAXISTA.ID);
           taxistaref.child("solicitudActual").setValue(solicitud.ID);
           taxistaref.child("estado").setValue("asignado");
       }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
switch (item.getItemId()){
    case R.id.action_historial:
        if (lineahistorial.getVisibility() == View.VISIBLE) {
            lineahistorial.setVisibility(View.GONE);
            lineamapa.setVisibility(View.VISIBLE);
            lineaperfil.setVisibility(View.GONE);
            //  lineafavoritos.setVisibility(View.GONE);
            getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            lineahistorial.setVisibility(View.VISIBLE);
            lineaperfil.setVisibility(View.GONE);
            lineamapa.setVisibility(View.GONE);
            // lineafavoritos.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle(getResources().getString(R.string.titulo_historial));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        break;
    case R.id.action_perfil:
        if (lineaperfil.getVisibility() == View.VISIBLE) {
            lineahistorial.setVisibility(View.GONE);
            lineamapa.setVisibility(View.VISIBLE);
            lineaperfil.setVisibility(View.GONE);
          //  lineafavoritos.setVisibility(View.GONE);
            getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            lineahistorial.setVisibility(View.GONE);
            lineaperfil.setVisibility(View.VISIBLE);
            lineamapa.setVisibility(View.GONE);
           // lineafavoritos.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle(getResources().getString(R.string.titulo_perfil));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        break;

    case android.R.id.home:
        lineahistorial.setVisibility(View.GONE);
        lineamapa.setVisibility(View.VISIBLE);
        lineaperfil.setVisibility(View.GONE);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        break;

    case R.id.action_salir:
        lanzarDialogoSalir();
        break;
}

        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putAll(bundleinicial);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        bundleinicial=savedInstanceState;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}
