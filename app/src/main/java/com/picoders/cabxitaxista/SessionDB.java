package com.picoders.cabxitaxista;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.provider.Settings;

/**
 * Created by juancho on 6/12/16.
 */

public class SessionDB extends SQLiteOpenHelper {
    Context context;
    private static int HORAS_SESSION=8;
    private static int MINUTOS_SESSION=HORAS_SESSION*60;
    public SessionDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE= "CREATE TABLE session ("+
                "idTaxi TEXT RIMARY KEY, "+
                "codigoEmpresa TEXT, " +
                "codigoRadio TEXT, " +
                "disponible TEXT, "+
                "marca TEXT, "+
                "modelo TEXT, "+
                "placa TEXT, "+
                "ciudad TEXT, "+
                "idTaxista TEXT,"+
                "documento TEXT,"+
                "imagen TEXT, "+
                "telefono TEXT, "+
                "longitud TEXT, "+
                "latitud TEXT, "+
                "ultimoLogin TEXT"+
                ")";

        db.execSQL(CREATE_LOGIN_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS session");
        this.onCreate(db);
    }

    public void borrarTabla(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS session");
        this.onCreate(db);
    }

    public void addRecord(Taxi taxi, Taxista taxista){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("idTaxi",taxi.ID);
        value.put("codigoEmpresa",taxi.CODIGOEMPRESA);
        value.put("codigoRadio",taxi.CODIGORADIO);
        value.put("disponible",taxi.DISPONIBLE);
        value.put("marca",taxi.MARCA);
        value.put("modelo",taxi.MODELO);
        value.put("placa",taxi.PLACA);
        value.put("ciudad",taxista.CIUDAD);
        value.put("idTaxista",taxista.ID);
        value.put("documento",taxista.DOCUMENTO);
        value.put("imagen",taxista.IMAGEN);
        value.put("telefono",taxista.TELEFONO);
        value.put("longitud",String.valueOf(taxi.LONGITUD));
        value.put("latitud",String.valueOf(taxi.LATITUD));
        value.put("ultimoLogin", String.valueOf(System.currentTimeMillis()));

        if (db.isOpen()){
            db.insert("session",null,value);
        }
        if(db != null && db.isOpen())
            db.close();
    }

    public Taxi getTaxi(){
            String query = "SELECT * FROM session";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query,null);
    Taxi taxi=null;
            if (cursor.moveToFirst()){

                    Bundle bundle = new Bundle();
                    for (String columna : cursor.getColumnNames())
                        bundle.putString(columna, cursor.getString(cursor.getColumnIndex(columna)));
                    String millislogin = bundle.get("ultimoLogin").toString();
                    if (Long.parseLong(millislogin) >= (System.currentTimeMillis() - (MINUTOS_SESSION * 60000)))
                        taxi = new Taxi(context, bundle, null);
                else
                        borrarTabla();

            }

            return taxi;
    }

    public Taxista getTaxista(){
        String query = "SELECT * FROM session";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);
        Taxista taxista=null;

            while (cursor.moveToNext()) {
                Bundle bundle = new Bundle();
                for (String columna : cursor.getColumnNames())
                    bundle.putString(columna, cursor.getString(cursor.getColumnIndex(columna)));

                String millislogin = bundle.get("ultimoLogin").toString();
                if (Long.parseLong(millislogin) >= (System.currentTimeMillis() - (MINUTOS_SESSION * 600000)))
                    taxista = new Taxista(context, bundle);

        }

        return taxista;
    }
}
