package com.picoders.cabxitaxista;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by juancho on 5/18/15.
 */

public class LoginDB extends SQLiteOpenHelper {
    public LoginDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE= "CREATE TABLE login ("+
                "documento TEXT RIMARY KEY, "+
                "password TEXT)";

        db.execSQL(CREATE_LOGIN_TABLE);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP TABLE IF EXISTS login");
        this.onCreate(db);
    }

    public void addRecord(String documento, String password){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value1 = new ContentValues();
        value1.put("documento",documento);
        value1.put("password",password);

        if (db.isOpen()){
            db.insert("login",null,value1);
                    }
        if(db != null && db.isOpen())
            db.close();
    }

    public String[] getRecord(){
        String result[] = new String[2]; //0=username  1=password

        String query = "SELECT * FROM login";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            int count=0;
            do{
                result[0]=cursor.getString(0);
                result[1]=cursor.getString(1);

            }while(cursor.moveToNext());
        }

        if(db != null && db.isOpen())
            db.close();


        return result;
    }
    public void droptable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM login");
        db.close();
    }
}
