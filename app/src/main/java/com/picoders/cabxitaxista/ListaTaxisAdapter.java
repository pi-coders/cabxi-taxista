package com.picoders.cabxitaxista;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by juancho on 17/11/16.
 */

public class ListaTaxisAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Taxi> lista;
    Context context;
    public ListaTaxisAdapter(Context context, List<Taxi> list){
        this.context=context;
        this.lista=list;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listataxisrow, parent, false);
        return new ListaTaxisHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Taxi taxi = lista.get(position);
        ListaTaxisHolder holder1 = (ListaTaxisHolder)holder;
        holder1.pintarLinea(context,taxi);

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
}
