package com.picoders.cabxitaxista;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by juancho on 4/12/16.
 */

public class ListaHistorialAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        List<Solicitud> lista;
        Context context;

        public ListaHistorialAdapter(Context context, List<Solicitud> list){
        this.context=context;
        this.lista=list;
        }
@Override
public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.historialrow, parent, false);
        return new ListaHistorialHolder(inflatedView);
        }

@Override
public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Solicitud solicitud = lista.get(position);
        ListaHistorialHolder holder1 = (ListaHistorialHolder) holder;
        holder1.pintarLinea(context,solicitud);

        }

@Override
public int getItemCount() {
        return lista.size();
        }
        }
