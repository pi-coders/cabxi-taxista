package com.picoders.cabxitaxista;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by juancho on 23/01/17.
 */

public class Narracion implements TextToSpeech.OnInitListener{
    private TextToSpeech textToSpeech;
    private Context context;

    public Narracion(Context context){
        this.context=context;
        textToSpeech=new TextToSpeech(context,this);
    }


    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS) {
            Locale locSpanish = new Locale("spa", "MEX");
            int result=textToSpeech.setLanguage(locSpanish);
            if(result==TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                Log.e("TTS","Language not supported");
            }else{
                Log.d("TTS","TextToSpeech Initialized Successfully");
            }
        }else{
            Log.e("TTS","Initializacion Error");
        }
    }

    public void decir(String texto){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21(cambiarPalabrasClaves(texto));
        } else {
            ttsUnder20(cambiarPalabrasClaves(texto));
        }
    }
    public void apagar(){
        textToSpeech.stop();
        textToSpeech.shutdown();
    }

    @SuppressWarnings("deprecation")
    private void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, map);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void ttsGreater21(String text) {
        String utteranceId=this.hashCode() + "";
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
    }

    private String cambiarPalabrasClaves(String texto){
        texto=texto.replace("Cl.","Calle");
        texto=texto.replace("C.","Calle");
        texto=texto.replace("Cr.","Carrera");
        texto=texto.replace("Cra.","Carrera");
        texto=texto.replace("Kr","Carrera");
        texto=texto.replace("K","Carrera");
        texto=texto.replace("Trans.","Transversal");
        texto=texto.replace("Tr.","Transversal");
        texto=texto.replace("Tv.","Transversal");
        texto=texto.replace("T.","Transversal");
        texto=texto.replace("Diag.","Diagonal");
        texto=texto.replace("Dg.","Diagonal");
        texto=texto.replace("D.","Diagonal");
        texto=texto.replace("Av.","Avenida");
        texto=texto.replace("A.","Avenida");
        texto=texto.replace("AK.","Avenida Carrera");
        texto=texto.replace("Ak.","Avenida Carrera");
        texto=texto.replace("AC.","Avenida Calle");
        texto=texto.replace("Autop.","Autopista");
        texto=texto.replace("Ap.","Autopista");
        texto=texto.replace("#"," número ");
        texto=texto.replace("-"," raya ");
        return texto;
    }
}
