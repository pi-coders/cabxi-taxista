package com.picoders.cabxitaxista;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by juancho on 14/12/16.
 */

public class ServicioReportePosicion extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks {
    Long LAST_UPDATE = Long.valueOf(0);
    DatabaseReference taxiref;
    Taxi taxi;
    Taxista taxista;
    int INTERVALO = 10 * 1000;//10 segundos
    LocationRequest locationRequest;
    GoogleApiClient googleApiClient;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Solicitud solicitud = (Solicitud) intent.getParcelableExtra("solicitud");
      try {
          taxi = intent.getParcelableExtra("taxi");
          taxista = (Taxista) intent.getExtras().getParcelable("taxista");
          taxiref = FirebaseDatabase.getInstance().getReference().child("taxis").child(taxi.ID);
          googleApiClient = new GoogleApiClient.Builder(this)
                  .addApi(LocationServices.API)
                  .addConnectionCallbacks(this)
                  .build();
          googleApiClient.connect();
          locationRequest = LocationRequest.create();
          locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
          locationRequest.setInterval(5000); // Update location every 5 second
      }catch (NullPointerException e){
          e.printStackTrace();
      }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onLocationChanged(Location location) {
        Long millis = System.currentTimeMillis();
        if (LAST_UPDATE <= millis - INTERVALO) {
            updateLocationInFirebase(location);
            LAST_UPDATE = millis;
        }
    }

    private void updateLocationInFirebase(Location location) {
        Map<String, Object> taskMap = new HashMap<String, Object>();
        taskMap.put("latitud", location.getLatitude());
        taskMap.put("longitud", location.getLongitude());
        taskMap.put("rumbo", location.getBearing());
        taskMap.put("ultimoReporte", System.currentTimeMillis());
        taxiref.updateChildren(taskMap);
        taxiref.child("idTaxista").setValue(taxista.ID);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        while(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        taxiref.child("idTaxi").setValue(null);
        taxiref.child("idTaxista").setValue(null);
        googleApiClient.disconnect();
    }
}
