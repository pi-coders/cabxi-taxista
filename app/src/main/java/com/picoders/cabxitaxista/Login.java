package com.picoders.cabxitaxista;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 28/10/16.
 */

public class Login extends Activity implements FirebaseAuth.AuthStateListener,View.OnFocusChangeListener,ListaTaxisHolder.AutorizedTaxiClickListener{

    private LinearLayout linealogin,lineacrear,lineacontrasenas,lineaescogetaxis,lineacargataxis,lineacargaverificar;
    private EditText editlogincedula,editlogincontrasena,editcrearcedula,editcreartelefono,contrasena1,contrasena2;
    TextView botonlogin,iracrear,botoncrear,iralogin,botonverificar,botoncrearcontrasena,botonvolver;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference rootref;
    Utilidades util;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    RecyclerView lineataxis;
    ListaTaxisAdapter listaTaxisAdapter;
    List<Taxi> LISTA_TAXIS_AUTORIZADOS=new ArrayList<>();
    Handler handler;
    List<Marca> LISTA_MARCAS=new ArrayList<>();
    Taxista TAXISTA;
    LoginDB logindb;
    SessionDB sessiondb;
    TextView nohaytaxis;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        linealogin = (LinearLayout) findViewById(R.id.linealogin);
        lineacrear = (LinearLayout) findViewById(R.id.lineacrear);
        lineacontrasenas = (LinearLayout) findViewById(R.id.lineacontrasenas);
        editlogincedula = (EditText) findViewById(R.id.editcedula);
       // editlogincedula.setOnFocusChangeListener(this);
        editlogincontrasena= (EditText) findViewById(R.id.editcontrasena);
     //   editlogincontrasena.setOnFocusChangeListener(this);
        editcrearcedula = (EditText) findViewById(R.id.editcrearcedula);
        //editcrearcedula.setOnFocusChangeListener(this);
        editcreartelefono = (EditText) findViewById(R.id.editcreartelefono);
        //editcreartelefono.setOnFocusChangeListener(this);
        contrasena1 = (EditText) findViewById(R.id.editcontrasena1);
        //contrasena1.setOnFocusChangeListener(this);
        contrasena2 = (EditText) findViewById(R.id.editcontrasena2);
        //contrasena2.setOnFocusChangeListener(this);
        botonlogin = (TextView) findViewById(R.id.botoningresar);
        botoncrear = (TextView) findViewById(R.id.botonverificar);
        botoncrearcontrasena = (TextView) findViewById(R.id.botoncrearcontrasena);
        iracrear = (TextView) findViewById(R.id.iracrear);
        iralogin = (TextView) findViewById(R.id.iralogin);
        lineataxis = (RecyclerView) findViewById(R.id.lineataxis);
        lineaescogetaxis = (LinearLayout) findViewById(R.id.lineaescogetaxis);
        lineacargataxis = (LinearLayout) findViewById(R.id.lineacargataxis);
        lineacargaverificar = (LinearLayout) findViewById(R.id.lineacargaverificar);
        botonvolver = (TextView) findViewById(R.id.boton_volver_login);
        nohaytaxis = (TextView) findViewById(R.id.textonohaytaxis);
        logindb = new LoginDB(this,"login",null,1);
        sessiondb = new SessionDB(this,"session",null,1);


        lineataxis.setLayoutManager(new LinearLayoutManager(this));
        listaTaxisAdapter = new ListaTaxisAdapter(this,LISTA_TAXIS_AUTORIZADOS);

        util=new Utilidades();
        handler=new Handler();
        MapsInitializer.initialize(getApplicationContext());

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.addAuthStateListener(this);
        checkPlayServices();
        firebaseAuth.signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                rootref = FirebaseDatabase.getInstance().getReference();
                getMarcas();
                String[] info = logindb.getRecord();
                Taxi tmptaxi = sessiondb.getTaxi();
                Taxista tmptaxista = sessiondb.getTaxista();
                if(info[0]!=null && (tmptaxi==null || tmptaxista == null)){
                    editlogincedula.setText(info[0]);
                    editlogincontrasena.setText(info[1]);
                    botonlogin.performClick();
                }else if(tmptaxi != null && tmptaxista!=null){
                    Intent intent = new Intent(Login.this,MainMap.class);
                    intent.putExtra("taxista",tmptaxista);
                    intent.putExtra("taxi",tmptaxi);
                   // intent.putExtras(tmptaxi.BUNDLE);
                    DatabaseReference taxiref = rootref.child("taxis").child(tmptaxi.ID);
                    taxiref.child("idTaxista").setValue(tmptaxista.ID);
                    taxiref.child("disponible").setValue(true);
                    startActivity(intent);
                    finish();
                }
                if(!task.isSuccessful())
                    task.getException().printStackTrace();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();


            }
        });
        botonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                botonlogin.setEnabled(false);
                final String cedula = editlogincedula.getText().toString();
                final String contrasena = editlogincontrasena.getText().toString();

                if(cedula.length()==0 || cedula.equals(getResources().getString(R.string.prompt_cedula))){
                    editlogincedula.setError(getResources().getString(R.string.error_cedula_invalida));
                }else if(contrasena.length()==0 || contrasena.equals(getResources().getString(R.string.prompt_password))){
                    editlogincontrasena.setError(getResources().getString(R.string.error_contrasena_invalida));
                }else{
                    DatabaseReference taxistasref = rootref.child("taxistas");
                    Query busquedaquery = taxistasref.orderByChild("documento").equalTo(cedula);

                    busquedaquery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            boolean isCedulaOnDatabase=false;
                            for(DataSnapshot snap: dataSnapshot.getChildren())
                            try {
                                if (snap.child("contrasena").getValue().toString().equals(util.MD5_Hash(contrasena))) {
                                    // login existoso
                                 final  Bundle bundle = new Bundle();
                                    for (DataSnapshot snap2 : snap.getChildren())
                                        bundle.putString(snap2.getKey(), snap2.getValue().toString());
                                    bundle.putString("idTaxista", snap.getKey());

                                    TAXISTA = new Taxista(Login.this,bundle);
                                    linealogin.setVisibility(View.GONE);
                                    lineaescogetaxis.setVisibility(View.VISIBLE);
                                    logindb.addRecord(cedula,contrasena);
                                    isCedulaOnDatabase=true;

                                    Bundle tempbundle = new Bundle();

                                  handler.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          obtenerListaTaxisAutorizados(bundle);
                                      }
                                  })  ;

                                 break;
                                } else {
                                    isCedulaOnDatabase=true;
                                    //TODO contraseña no es la misma
                                    editlogincontrasena.setError(getString(R.string.error_contrasena_invalida_login));
                                    botonlogin.setEnabled(true);
                                }
                            }catch (NullPointerException e){
                                // usuario no existe o no tiene contreaseña
                                editlogincedula.setError(getString(R.string.error_cedula_no_encontrada));

                            }

                            if(!isCedulaOnDatabase)
                                editlogincedula.setError(getString(R.string.error_cedula_no_encontrada));

                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {


                        }
                    });


                }
            }
        });

        botoncrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lineacargaverificar.setVisibility(View.VISIBLE);
                 final String telefono = editcreartelefono.getText().toString();
                String cedula = editcrearcedula.getText().toString();

                rootref.child("taxistas").orderByChild("documento").equalTo(cedula).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean isCedulaOnDatabase=false;
                        for(DataSnapshot snap:dataSnapshot.getChildren()){
                            if(snap.child("telefono").getValue().toString().equals(telefono)){
                                Bundle bundle = new Bundle();
                                for(DataSnapshot cadasnap:snap.getChildren())
                                    bundle.putString(cadasnap.getKey(),cadasnap.getValue().toString());
                                bundle.putString("idTaxista",snap.getKey());

                                TAXISTA = new Taxista(Login.this,bundle);
                                isCedulaOnDatabase=true;
                                if(snap.child("contrasena").getValue()==null){
                                    lineacontrasenas.setVisibility(View.VISIBLE);
                                    lineacargaverificar.setVisibility(View.GONE);
                                    lineacrear.setVisibility(View.GONE);

                                }else{
                                    lineacargaverificar.setVisibility(View.GONE);
                                    editcreartelefono.setError(getResources().getString(R.string.error_contraseña_ya_existe));
                                }

                            }else {
                                lineacargaverificar.setVisibility(View.GONE);
                                editcreartelefono.setError(getResources().getString(R.string.error_credenciales_no_concuerdan));
                                isCedulaOnDatabase=true;
                            }

                        }
                        if(!isCedulaOnDatabase){
                            lineacargaverificar.setVisibility(View.GONE);
                            lineacrear.setVisibility(View.VISIBLE);
                            editcrearcedula.setError(getString(R.string.error_cedula_no_existe));
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });

        botoncrearcontrasena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass1 = contrasena1.getText().toString();
                String pass2 = contrasena2.getText().toString();

                if(pass1.length()==0)
                    contrasena1.setError(getString(R.string.error_contrasena_invalida));
                else if(pass2.length()==0)
                    contrasena2.setError(getString(R.string.error_contrasena_invalida));
                else if (!pass1.equals(pass2))
                    contrasena2.setError(getString(R.string.error_contrasenas_diferentes));
                else{
                    lineacontrasenas.setVisibility(View.GONE);
                    linealogin.setVisibility(View.VISIBLE);
                    rootref.child("taxistas").child(TAXISTA.ID).child("contrasena").setValue(util.MD5_Hash(pass1));
                    editlogincedula.setText(TAXISTA.DOCUMENTO);
                    editlogincontrasena.setText(pass1);
                    botonlogin.performClick();


                }
            }
        });

        iracrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linealogin.setVisibility(View.GONE);
                lineacrear.setVisibility(View.VISIBLE);
                lineaescogetaxis.setVisibility(View.GONE);
            }
        });
        iralogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linealogin.setVisibility(View.VISIBLE);
                lineacrear.setVisibility(View.VISIBLE);
                lineaescogetaxis.setVisibility(View.GONE);
            }
        });

        botonvolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logindb.droptable();
                sessiondb.borrarTabla();
                editlogincedula.setText("");
                editlogincontrasena.setText("");
                lineaescogetaxis.setVisibility(View.GONE);
                linealogin.setVisibility(View.VISIBLE);
                LISTA_TAXIS_AUTORIZADOS.clear();
                listaTaxisAdapter.notifyDataSetChanged();
                botonlogin.setEnabled(true);

            }
        });

    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user!=null){
            System.out.println("Signed in anonimouly");

        }else{
            System.out.println("Signed Out");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

       if(v==editlogincedula)
           if (hasFocus)
               if(editlogincedula.getText().toString().equals(getResources().getString(R.string.prompt_cedula)))
                   editlogincedula.setText("");
           else
                   if(editlogincedula.getText().toString().equals(""))
                       editlogincedula.setText(getResources().getString(R.string.prompt_cedula));

       else if(v==editlogincontrasena)
                       if(hasFocus)
                           if(editlogincontrasena.getText().toString().equals(getResources().getString(R.string.prompt_password))){
                               editlogincontrasena.setText("");
                           }


    }
    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }

        return true;
    }
    private void obtenerListaTaxisAutorizados(final Bundle bundle1){
        lineataxis.setAdapter(listaTaxisAdapter);
        DatabaseReference autorizadosref = rootref.child("taxis");
        Query querytaxistasautorizados=autorizadosref.orderByChild("conductoresAutorizados/"+bundle1.getString("idTaxista")+"/disponible").startAt(true).endAt(true);

        querytaxistasautorizados.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                        Bundle bundle = new Bundle();
                        for (DataSnapshot snap2 : dataSnapshot.getChildren())
                            bundle.putString(snap2.getKey(), snap2.getValue().toString());
                        bundle.putString("idTaxi", dataSnapshot.getKey());
                        bundle.putString("imagenMarca",getImageFromMarcasList(bundle.getString("marca")));
                    bundle.putString("nombreMarca",getNombreFromMarcasList(bundle.getString("marca")));

                        String idTaxista = bundle.getString("idTaxista");
                    String ultimoLogin = bundle.getString("ultimoLogin");
                        Taxi taxi = new Taxi(Login.this, bundle, null);
                        if(idTaxista==null) {
                            LISTA_TAXIS_AUTORIZADOS.add(taxi);
                            listaTaxisAdapter.notifyDataSetChanged();
                            nohaytaxis.setVisibility(View.GONE);
                            lineacargataxis.setVisibility(View.GONE);
                        }else if(ultimoLogin!=null){
                            Long templong = Long.parseLong(ultimoLogin);
                            if(templong <= System.currentTimeMillis()-360000){
                                LISTA_TAXIS_AUTORIZADOS.add(taxi);
                                listaTaxisAdapter.notifyDataSetChanged();
                                lineacargataxis.setVisibility(View.GONE);
                                nohaytaxis.setVisibility(View.GONE);
                            }else{
                                if(LISTA_TAXIS_AUTORIZADOS.size()==0){
                                    nohaytaxis.setVisibility(View.VISIBLE);
                                    lineacargataxis.setVisibility(View.GONE);
                                }
                            }
                        }else if(ultimoLogin==null){
                            LISTA_TAXIS_AUTORIZADOS.add(taxi);
                            listaTaxisAdapter.notifyDataSetChanged();
                            lineacargataxis.setVisibility(View.GONE);
                            nohaytaxis.setVisibility(View.GONE);
                        }


                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public String getImageFromMarcasList(String idMarca){
        for(Marca marca : LISTA_MARCAS)
            if(marca.ID.equals(idMarca))
                return marca.LOGO;

        return null;
    }
    public String getNombreFromMarcasList(String idMarca){
        for(Marca marca:LISTA_MARCAS)
            if(marca.ID.equals(idMarca))
                return marca.NOMBRE;
        return null;
    }
    private void getMarcas(){
        rootref.child("marcas").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Bundle bundle = new Bundle();
                for(DataSnapshot snap:dataSnapshot.getChildren())
                    bundle.putString(snap.getKey(),snap.getValue().toString());
                bundle.putString("idMarca",dataSnapshot.getKey());

                Marca marca = new Marca(bundle);
                LISTA_MARCAS.add(marca);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onAutorizedTaxiClicked(Taxi taxi) {
        Intent intent = new Intent(this,MainMap.class);
        intent.putExtra("taxista",TAXISTA);
        intent.putExtra("taxi",taxi);
        intent.putExtras(taxi.BUNDLE);
        sessiondb.borrarTabla();
        sessiondb.addRecord(taxi,TAXISTA);
        DatabaseReference taxiref = rootref.child("taxis").child(taxi.ID);
        taxiref.child("idTaxista").setValue(TAXISTA.ID);
        taxiref.child("disponible").setValue(true);
        taxiref.child("ultimoLogin").setValue(System.currentTimeMillis());
        startActivity(intent);
        finish();

    }

    private class Marca {
        String LOGO,NOMBRE,ID;
        public Marca(Bundle bundle){
            this.LOGO = bundle.getString("logo").replace(" ","%20");
            this.NOMBRE = bundle.getString("nombre");
            this.ID = bundle.getString("idMarca");
        }
    }
}
